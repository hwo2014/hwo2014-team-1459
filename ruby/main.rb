require 'json'
require 'socket'
require_relative 'ai.rb'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class NoobBot
  
  def initialize(server_host, server_port, bot_name, bot_key, trackname=nil)
    tcp = TCPSocket.open(server_host, server_port)
    tcp.setsockopt(Socket::IPPROTO_TCP, Socket::TCP_NODELAY, 1)
    play(bot_name, bot_key, trackname, tcp)
  end

  private

  def play(bot_name, bot_key, trackname, tcp)
    start_msg = trackname.nil? ? join_message(bot_name, bot_key) : create_race_message(bot_name, bot_key, trackname)
    tcp.puts start_msg
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    all_messages = []
    while json = tcp.gets
      next unless json.valid_json?
      message = JSON.parse(json)
      puts JSON.pretty_generate message
      msgType = message['msgType']
      msgData = message['data']
      case msgType
        when 'carPositions'
          @tick = message["gameTick"]
          Util.logger :WARNING => @tick if @tick.nil?
          result = AI.calculate msgData
          Util.dump result
          if result.has_key?(:switch)
              instr = switch_message result[:switch].to_s.capitalize
              Util.logger :switch => result[:switch].to_s
          else
              instr = throttle_message result[:throttle]  
              Util.logger :throttle => result[:throttle]
          end
          Util.dump instr
          tcp.puts instr
        else

          case msgType
            when 'join'
              puts 'Joined'
            when 'yourCar'
              AI.car = Car.new(msgData["name"], msgData["color"])
            when 'gameInit'
              racesession = msgData["race"]["raceSession"]
              AI.qualifying = true unless racesession.has_key?("quickRace")
              if racesession.has_key?("quickRace")
                AI.qualifying = racesession["quickRace"]
              end
             
              if AI.qualifying == true
                racetrack = msgData["race"]["track"]
                mytrack = Track.new(racetrack["id"], racetrack["name"])
                mytrack.lanes_info racetrack["lanes"]
                mytrack.track_info(racetrack["pieces"])
                AI.track = mytrack
              end
            when 'gameStart'
              puts 'Race started'
              @tick = message["gameTick"]
              AI.stats = {}
              AI.init_stats
              tcp.puts ping_message
            when 'crash'
              puts 'Someone crashed'
            when 'gameEnd'
              puts 'Race ended'
              AI.dump_stats
            when 'error'
              puts "ERROR: #{msgData}"
          end
          puts "Recieved #{msgType}"
          all_messages << msgType unless all_messages.include?(msgType)
          puts all_messages
          #tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def create_race_message(bot_name, bot_key, track_name)
    botId = {:name => bot_name, :key => bot_key}
    track = track_name
    pwd = "abc"
    cars = 1
    payload = {:botId => botId, :trackName => track, :password => pwd, :carCount => cars}
    make_msg("createRace", payload)
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def switch_message dir
    make_msg("switchLane", dir)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msgType, data)
    hash = {:msgType => msgType, :data => data, :tick => @tick}
    msg = JSON.generate(hash)
    msg
  end
end

#NoobBot.new(server_host, server_port, bot_name, bot_key, "pentag")
NoobBot.new(server_host, server_port, bot_name, bot_key)
