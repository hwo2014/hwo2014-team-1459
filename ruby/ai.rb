
class Util
  def self.logger opts
    log "\n" if opts.has_key?(:new_line)
    opts.each do |k, v|
      next if k == :new_line
      log "#{k.to_s} = #{v}"
    end
  end

  def self.dump str
    log str
  end
  
  def self.log str
    puts str
    open("racelog.log", "a") {|f| f.puts str}
  end

  def self.deg_to_rad(deg)
    deg * (3.14 / 180)
  end

  def self.rad_to_deg(rad)
    rad * (180 / 3.14)
  end

  def self.delta arr, depth
    depth.times do
      return [0] if arr.length < 2
      arr = arr.each_cons(2).map {|elem| elem.last - elem.first}
    end
    arr
  end
  
  def self.clean data
    data.each {|k, v| v.shift(v.length - 60) if v.length > 70}
  end
end

class String
   def valid_json?
      begin
        JSON.parse(self)
        return true
      rescue
        return false
      end
   end
end

class Car
   attr_reader :name, :color
   attr_reader :velocity, :acceleration, :time
   attr_reader :max_throttle, :max_power, :piece_changed
   attr_accessor :throttle, :fmax, :data

   def initialize(name, color)
       @name = name
       @color = color
       @distance = 0.0
       @time = 0
       @velocity = 0.0
       @acceleration = 0.0

       @max_power = 0.0
       @max_throttle = 0.0
       @throttle = 0.0

       @current_piece = -1
       @piece_changed = false
       @lap = 0
       @lane_index = 0
       @switches = {}

       @fmax = -1

       @data = {:distance => [0], :angles => [0]}
   end

   def update distance, index, dist
       @time += 1
       @distance = dist
       @distance += calc_total_dist(index - 1, distance) unless index.zero? && @lap.zero?
       @data[:distance] << @distance
       @velocity = Util.delta(@data[:distance], 1).last
       return if @velocity < 0 # Hopefully this never happens

       # Calculate constant params (power, )
       if (@time < 10)
          @max_throttle = [@max_throttle, @throttle].max
          power = 0.0
          power = @max_throttle / @acceleration unless @acceleration.zero?
          @max_power = [@max_power, power].max
       end

       # Calculate regular parameters (accel, vel)
       @acceleration = Util.delta(@data[:distance], 2).last
       delta_accel = Util.delta(@data[:distance], 3).last

       # Print stats
       stats "Delta accel" => delta_accel

       Util.clean @data
   end


   def stopping_dist vmax
       # s = (v^2 - u^2) / 2a
       return -100 if @velocity < vmax
       dist = (vmax) ** 2
       dist -= (@velocity ** 2)
       accel = @acceleration - (@max_throttle / @max_power)
       (dist / (2 * accel)).abs
   end

   def calc_total_dist index, dist
      total = 0
      index = dist.length - 1 if index < 0
      (0..index).each {|i| total += dist[i][@switches[i]]}
      total
   end

   def update_pos index, lap, lane
      @piece_changed = true if @current_piece != index
      @piece_changed = false if @current_piece == index
      @current_piece = index
      @lap = lap
      @lane_index = lane
      @switches[index] = lane
      if @piece_changed
         Util.logger :fmax => @fmax
      end
   end

   def stats arg=nil
       Util.logger :new_line => true
       Util.logger :tick => @time, :distance => @distance
       Util.logger :velocity => @velocity, :accel => @acceleration
       return if arg.nil?
       arg.each do |k, v|
          Util.logger k.to_sym => v
       end
   end
end

class Track
   attr_reader :id, :name, :pieces, :lanes
   attr_reader :tot_dist, :dist, :switch_index

   def initialize(id, name)
       @id = id
       @name = name
       @pieces = []
       @tot_dist = []
       @dist = []
       @lanes = {}
   end

   def track_info(pieces)
      pindex = 0
      pieces.each do |p|
         @pieces << p
         @pieces[pindex][:vmax] = 1000
         @pieces[pindex][:max_angle] = 0.0
         @pieces[pindex][:velocity] = 0.0
         @tot_dist[pindex] = {}
         @dist[pindex] = {}

         @lanes.each_key do |index| 
            dist = 0
            if p.has_key?("radius")
               radius = p["radius"]
               radius += (dir(p["angle"]) == :right) ? -@lanes[index] : @lanes[index]
               dist = (2 * 3.14 * radius * p["angle"].abs) / 360
            end
            dist = p["length"] if p.has_key?("length")
            @dist[pindex][index] = dist
            curr = @tot_dist[pindex - 1].nil? ? 0 : @tot_dist[pindex - 1][index]
            curr = 0 if curr.nil?
            @tot_dist[pindex][index] = dist + curr
         end
         pindex += 1
      end
      Util.dump @tot_dist
   end

   def lanes_info(lanes)
      lanes.each {|lane| @lanes[lane["index"]] = lane["distanceFromCenter"]}
      Util.dump @lanes
   end

   def straight?(pindex)
      !@pieces[pindex % @pieces.length].has_key?("radius")
   end

   def curve?(pindex)
     !straight?(pindex)
   end

   def switch?(pindex)
      @pieces[pindex % @pieces.length].has_key?("switch")
   end

   def next_switch(pindex)
      turns = {}
      turns[:right] = turns[:left] = 0
      till(:switch, pindex) do |index|
          next if straight?(index)
          turns[dir @pieces[index]["angle"]] += @pieces[index]["radius"]
      end
      return nil if turns[:right] == turns[:left]
      turns.max_by {|k, v| v}.first
   end

   def dump_piece(pindex)
      Util.dump @pieces[pindex % @pieces.length]
   end
    
   def dir(angle)
      angle >= 0 ? :right : :left
   end

   def till(cond, index)
      index = index % @pieces.length
      curr = index
      loop do
         return if switch?(curr) && curr != index && cond == :switch # breaks if only one switch in lap
         yield curr
         return if curve?(curr) && cond == :turn
         curr += 1
         curr = 0 if curr == @pieces.length
      end
   end
end

class AI
   class << self
      attr_accessor :car, :track, :qualifying
      attr_accessor :stats 

      def calculate data
         if (@car.time == 11)
            Util.logger :max_power => @car.max_power, :max_throttle => @car.max_throttle
         end
         carpos = get_carpos data
         switch = decide_on_switch carpos 
         throttle = decide_throttle carpos
         result = {:throttle => throttle}
         result[:switch] = switch unless switch.nil?
         result
      end

      def init_stats
        (0...@track.pieces.length).each { |i| 
          @stats[i] = {}
          @stats[i][:max_angle] = []
          @stats[i][:vmax] = []
        }
        #stats = {1 => {:max_angle => [], :vmax => []}}
      end

      def dump_stats
        @stats.each_key do |k|
           Util.dump "#{k}: #{@stats[k][:vmax]}"
        end
        
        Util.logger :new_line => true
        @stats.each_key do |k|
           arr = @stats[k][:max_angle].map {|i| i.to_s.split('.')}
           arr = arr.map {|i| [i.first.rjust(2, '0'), i.last.ljust(3, '0')]}.map {|i| i.join('.')}
           Util.dump "#{k}: #{arr}"
        end
      end

      private
      def decide_throttle carpos
         
         pindex = carpos["piecePosition"]["pieceIndex"]
         pdist = carpos["piecePosition"]["inPieceDistance"]
         lane = carpos["piecePosition"]["lane"]["startLaneIndex"]
         lap = carpos["piecePosition"]["lap"]
         angle = carpos["angle"]
         Util.logger :index => pindex
         Util.logger :angle => angle
         @car.data[:angles] << angle
         print_pieces pindex

         @car.update_pos pindex, lap, lane
         @car.update @track.dist, pindex, pdist

         vmax = @track.pieces[pindex][:vmax]
         update_lap_details if lap_changed? lap, pindex
         # update track details -- refactor
         @track.pieces[pindex][:crash] = true if crashed?
         max_angle = @track.pieces[pindex][:max_angle]
         m_angle = [max_angle, angle.abs].max
         if m_angle > @track.pieces[pindex][:max_angle]
            @track.pieces[pindex][:max_angle] = m_angle
            @track.pieces[pindex][:velocity] = @car.velocity
         end
         
         @car.throttle = 1
         @car.throttle = calculate_pre_fmax(pindex) if @car.fmax < 1
         @car.throttle = 0.7 if @car.fmax >= 0 && @track.curve?(pindex)
         return @car.throttle if @car.time <= 10 # We need this time to collect data
         dtc = dist_to_curve pindex, pdist, lane
         @car.throttle = 0 if dtc.first < @car.stopping_dist(@track.pieces[dtc.last][:vmax]) + 15
         @car.throttle = 0 if @track.curve?(pindex) && @car.velocity > vmax
         @car.throttle = 0 if angle.abs >= 45
         @car.throttle = 0 if angle.abs >= angle_limit(pindex)
         angle_delta = @car.data[:angles].map {|i| i.abs}
         Util.logger :angle_delta => Util.delta(angle_delta, 1).last
         @car.throttle = 0 if Util.delta(angle_delta, 1).last >= 4.0

         if angle_reducing? && angle.abs > 45
             Util.logger :freeze_angle => angle
             @track.pieces[pindex][:freeze] = true
         end
         
         calculate_fmax pindex, angle if @car.fmax < 0
         
         Util.logger :dtc => dtc.first, :vmax => vmax
         Util.logger :stopping_dist => @car.stopping_dist(vmax)

         @car.throttle
      end

      def calculate_fmax pindex, angle
         return nil unless @car.fmax < 0 && @track.curve?(pindex)
         return nil unless angle.abs > 0
         radius = @track.pieces[pindex]["radius"]
         w = Util.rad_to_deg(@car.velocity / radius)
         w = w - angle.abs
         vmax = Util.deg_to_rad(w) * radius
         @car.fmax = (vmax ** 2) / radius
         turns = (0...@track.pieces.length).select {|i| @track.curve?(i)}
         turns.each do |index|
            radius = @track.pieces[index]["radius"]
            @track.pieces[index][:vmax] = ((@car.fmax * radius) ** 0.5) 
         end
      end

      def calculate_pre_fmax index
         turns = turn_sequence
         init_throttle = 0.5
         turns.each do |arr|
            return [init_throttle, 1].min if arr.include?(index) || index < arr.first
            init_throttle += 0.1
         end
         [init_throttle, 1].min
      end

      def max_slip_velocity index, angle, delta_angle, delta_delta_angle
         return nil unless delta_angle.abs > 0.0

         ticks = 0
         angle = angle.abs
         while angle < 50
            delta_angle += delta_delta_angle.abs
            angle += delta_angle.abs
            ticks += 1
         end
         Util.logger :ticks => ticks
         # v = u + at
         accelerations = Util.delta(@car.data[:distance], 2).map {|i| i.abs}
         accelerations = accelerations[-5..-1] unless accelerations.length < 10
         accel = accelerations.min
         @track.pieces[index][:vmax] = @car.velocity + (accel.abs * (ticks / 2))
         Util.logger :accel => accel.abs, :ticks => (ticks / 2)
         @track.pieces[index][:vmax] -= 0.4
      end

      def angle_reducing?
        return false if @car.data[:angles].length < 7
        angles = @car.data[:angles].map {|i| i.abs}
        Util.delta(angles, 1)[-5..-1].select {|i| i < 0}.length == 5
      end

      def update_lap_details
        (0...@track.pieces.length).each {|i| @stats[i][:max_angle] << @track.pieces[i][:max_angle].round(3)}
        update_max_angles
        update_all_vmax
        update_stats
      end

      def update_all_vmax
        arr = turn_sequence
        Util.logger :arr => arr
        arr.each do |seq|
          Util.logger :seq => seq
          crash_vmax = 1000
          s = seq.select {|i| @track.pieces[i][:vmax] < 1000}
          next if s.length.zero?
          crashes = s.select {|i| @track.pieces[i].has_key?(:crash)}
          Util.logger :crashes => crashes.map {|i| @track.pieces[i][:velocity]}
          if crashes.length > 0
             Util.logger :crash => crashes
             crash_vmax = crashes.map {|i| @track.pieces[i][:velocity]}.min - 0.7
             crashes.each {|i| @track.pieces[i].delete(:crash)}
          end
          avg_vmax = s.map {|i| [@track.pieces[i][:velocity], @track.pieces[i][:max_angle]]}.max_by {|i| i.last}.first
          #avg_vmax = s.map {|i| @track.pieces[i][:vmax]}.reduce(&:+) / s.length
          avg_vmax = [avg_vmax, crash_vmax].min
          freeze = s.select {|i| @track.pieces[i].has_key?(:freeze)}
          Util.logger :freeze => freeze.map {|i| [@track.pieces[i][:velocity], @track.pieces[i][:max_angle]]}
          Util.logger :s => s.map {|i| [@track.pieces[i][:velocity], @track.pieces[i][:max_angle]]}
          
          if freeze.length > 0 && crashes.length.zero?
            avg_vmax = freeze.map {|i| @track.pieces[i][:velocity]}.max
          end
          freeze.map {|i| @track.pieces[i].delete(:freeze)}
          avg_vmax = 1000 if avg_vmax == 0.0
          seq.each {|i| @track.pieces[i][:vmax] = avg_vmax}
          Util.logger :avg_vmax => avg_vmax
        end
        (0...@track.pieces.length).each {|i| @track.pieces[i][:max_angle] = 0.0}
      end

      def update_max_angles
        angles = (0...@track.pieces.length).select {|i| @track.pieces[i][:max_angle] < 45}
        angles.each do |i|
            speed_bump = 0.0
            case @track.pieces[i][:max_angle]
            when 0..10
              speed_bump = 1.0
            when 11..20
              speed_bump = 0.8
            when 21..30
              speed_bump = 0.4
            when 31..45
              speed_bump = 0.2
            end
            @track.pieces[i][:velocity] += speed_bump unless @track.pieces[i][:velocity] == 0.0
            @track.pieces[i][:vmax] += speed_bump if @track.pieces[i][:velocity] == 0.0
        end
      end

      def turn_sequence
        turns = (0...@track.pieces.length).select {|i| @track.curve?(i)}
        #turns << (0...@track.pieces.length).select {|i| @track.curve?(i + 1)}
        #turns << (0...@track.pieces.length).select {|i| @track.curve?(i - 1)}
        turns = turns.flatten.uniq.sort
        #turns.flatten!.uniq!.sort!
        turns.unshift(0).each_cons(2).slice_before{|m, n| m + 1 < n}.map{|a| a.map(&:last)}
      end

      def update_stats
        (0...@track.pieces.length).each {|i| @stats[i][:vmax] << ([@track.pieces[i][:vmax], 1000].min).round(3)}
        #(0...@track.pieces.length).each {|i| @stats[i][:max_angle] << @track.pieces[i][:max_angle].round(3)}
      end


      def lap_changed? lap, pindex
         lap > 0 && @car.piece_changed && pindex.zero?
      end

      def crashed?
         if @car.data[:distance].length > 10
            return true if Util.delta(@car.data[:distance][-5..-1], 1).reduce(&:+).zero?
         end
         false
      end

      def angle_limit index
        return 100 if @stats[index][:max_angle].length < 2
        last_angle = @stats[index][:max_angle][-2]
        return [last_angle + 5, 50].min if @track.curve?(index) || last_angle > 40
        return 100
      end

      def decide_on_switch carpos
         return nil unless @car.piece_changed
         pindex = carpos["piecePosition"]["pieceIndex"]
         return nil unless @track.switch?(pindex + 1)
         @track.next_switch pindex + 1
      end

      def dist_to_curve pindex, dist, lane
        return [9999, pindex] if @track.curve?(pindex)
        total = @track.dist[pindex][lane] - dist
        @track.till(:turn, pindex + 1) do |index|
            return [total, index] if @track.curve?(index)
            total += @track.dist[index][lane]
        end
      end

      def get_carpos data
         carpos = {}
         data.each do |msg|
            next unless msg["id"]["color"] == @car.color
            carpos = msg
            break
         end
         carpos
      end

      def print_pieces pindex
         @track.dump_piece pindex
         @track.dump_piece pindex + 1
      end
   end
end
